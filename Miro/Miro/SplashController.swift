//
//  SplashController.swift
//  Miro
//
//  Created by waqar on 26/03/2021.
//

import UIKit
import LanguageManager_iOS

class SplashController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let selectedLanguage: Languages = .en
        LanguageManager.shared.setLanguage(language: selectedLanguage)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5){
            
            let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
            mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(mainActivity, animated: true, completion: nil)
        }
    }
}
