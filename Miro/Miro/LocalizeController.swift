//
//  LocalizeController.swift
//  Miro
//
//  Created by waqar on 27/03/2021.
//

import UIKit
import CoreLocation
import GoogleMaps
import FirebaseDatabase
import LanguageManager_iOS

class LocalizeController: UIViewController , CLLocationManagerDelegate  {

    @IBOutlet weak var btnContinue: UIButton!
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var lblTitle2: UILabel!
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let ref = Database.database().reference(withPath: "ainex-sports-default-rtdb")
        let username = UserDefaults.standard.string(forKey: "username") ?? ""
        let timeStamp = Date.currentTimeStamp
        let timeStampString = String(timeStamp)
        let movieRefAvengers = ref.child(String(timeStamp))

        let dictMovieRefAvengers: [String: Any] = ["username":username,"latitude":locValue.latitude, "longitude":locValue.longitude]
        
        movieRefAvengers.setValue(dictMovieRefAvengers) {
          (error:Error?, ref:DatabaseReference) in
          if let error = error {
            print("Data could not be saved: \(error).")
          } else {
            print("Data saved successfully!")
          }
        }

        let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        mainActivity.latitude = locValue.latitude
        mainActivity.longitude = locValue.longitude
        UserDefaults.standard.set(locValue.latitude, forKey: "latitude")
        UserDefaults.standard.set(locValue.longitude, forKey: "longitude")
        self.present(mainActivity, animated: true, completion: nil)
    }

    @IBAction func btnContinueClick(_ sender: Any) {
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
                
//        let latitude = UserDefaults.standard.double(forKey: "latitude") ?? 0.0
//        let longitude = UserDefaults.standard.double(forKey: "longitude") ?? 0.0
//        
//        let ref = Database.database().reference(withPath: "ainex-sports-default-rtdb")
//        let username = UserDefaults.standard.string(forKey: "username") ?? ""
//        let timeStamp = Date.currentTimeStamp
//        let timeStampString = String(timeStamp)
//        let movieRefAvengers = ref.child(String(timeStamp))
//
//        let dictMovieRefAvengers: [String: Any] = ["username":username,"latitude":latitude, "longitude":longitude]
//        
//        movieRefAvengers.setValue(dictMovieRefAvengers) {
//          (error:Error?, ref:DatabaseReference) in
//          if let error = error {
//            print("Data could not be saved: \(error).")
//          } else {
//            print("Data saved successfully!")
//          }
//        }
//        if(latitude != 0.0){
//            let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//            mainActivity.modalPresentationStyle = .fullScreen
//            mainActivity.latitude = latitude
//            mainActivity.longitude = longitude
//            self.present(mainActivity, animated: true, completion: nil)
//        }
//        let locationManager = CLLocationManager()
//        locationManager.requestAlwaysAuthorization()
//        if(hasLocationPermission()){
            
//        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let locationManager = CLLocationManager()
//        locationManager.requestAlwaysAuthorization()
//        locationManager.requestWhenInUseAuthorization()
//
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            locationManager.startUpdatingLocation()
//        }
        
        if(LanguageManager.shared.currentLanguage == .ar){
            lblTitle1.text = "استخدم موقعك لاكتشاف القصص والفرق واللاعبين في منطقتك"
            lblTitle2.text = "الرجاء السماح في مربع الحوار التالي لتلقي التوصيات المحلية"
            lblHeader.text = "حدِّد تجربتك"
            btnContinue.setTitle("استمر", for: .normal)
        }else{
            lblTitle1.text = "Use your location to discover stories , teams and players in your area"
            lblTitle2.text = "Please allow in the following dialog to receive local recommendations"
            lblHeader.text = "Localize your experience"
            btnContinue.setTitle("CONTINUE", for: .normal)
        }

    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        let manager = CLLocationManager()
        
        if CLLocationManager.locationServicesEnabled() {
            print(manager.authorizationStatus)
            switch manager.authorizationStatus {
            case .denied:
                hasPermission = true
                print("HH: kCLAuthorizationStatusDenied")
            case .restricted:
                hasPermission = true
                print("HH: kCLAuthorizationStatusRestricted")
            case .authorizedAlways:
                hasPermission = true
                print("HH: kCLAuthorizationStatusAuthorizedAlways")
            case .authorizedWhenInUse:
                hasPermission = true
                print("HH: kCLAuthorizationStatusAuthorizedWhenInUse")
            case .notDetermined:
                hasPermission = true
                print("HH: kCLAuthorizationStatusNotDetermined")
            @unknown default:
                    break
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }

}
extension Date {
    static var currentTimeStamp: Int64{
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}
