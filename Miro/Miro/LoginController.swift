//
//  LoginController.swift
//  Miro
//
//  Created by waqar on 26/03/2021.
//

import UIKit
import Firebase
import GoogleSignIn
import LanguageManager_iOS

class LoginController: UIViewController, GIDSignInDelegate {
    
    @IBOutlet weak var gmail_btn: UIButton!
    @IBOutlet weak var pickerPhone: UIView!
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var txtphone: UITextField!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnOneTime: UIButton!
    
    @IBOutlet weak var lblWelcome: UILabel!
    var verificationId = ""
    @IBAction func PhoneLogin(_ sender: Any) {
        pickerPhone.isHidden = false
        viewPicker.isHidden = false
        txtphone.attributedPlaceholder = NSAttributedString(string: "Phone Number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        if(LanguageManager.shared.currentLanguage == .ar){
            self.btnPhone.setTitle("استمر", for: .normal)
        }else{
            self.btnPhone.setTitle("CONTINUE", for: .normal)
        }
        
    }
    var phoneNumber = ""
    @IBAction func btnContinueClick(_ sender: Any) {
        
        if(btnPhone.currentTitle == "CONTINUE"){
            if(txtphone.text != ""){
                phoneNumber = txtphone.text!
                PhoneAuthProvider.provider().verifyPhoneNumber(txtphone.text!, uiDelegate: nil) { (verificationID, error) in
                     if let error = error {
                       print(error.localizedDescription)
                       return
                     }
                    self.verificationId = verificationID ?? ""
                    self.txtphone.text = ""
                    self.txtphone.attributedPlaceholder = NSAttributedString(string: "Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                    if(LanguageManager.shared.currentLanguage == .ar){
                        self.btnPhone.setTitle("إرسال", for: .normal)
                    }else{
                        self.btnPhone.setTitle("SUBMIT", for: .normal)
                    }
                }
            }
        }else{
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationId,
                verificationCode: txtphone.text!)

            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    return
                }
                UserDefaults.standard.set(self.phoneNumber, forKey: "username")
                let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "LocalizeController") as! LocalizeController
                mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(mainActivity, animated: true, completion: nil)
           }
        }
    }
    
    @IBAction func GoogleLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!,present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!,dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                       withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let idToken = user.authentication.idToken ?? ""
            let fullName = user.profile.name ?? ""
            let email = user.profile.email ?? ""
            UserDefaults.standard.set(email, forKey: "username")

        } else {
        }
        if let error = error {
            // ...
            return
          }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                            accessToken: authentication.accessToken)
        
        
        Auth.auth().signIn(with: credential) { (authResult, error) in

//          if let error = error {
//            let authError = error as NSError
//            if (authError.code == AuthErrorCode.secondFactorRequired.rawValue) {
//              // The user is a multi-factor user. Second factor challenge is required.
//              let resolver = authError.userInfo[AuthErrorUserInfoMultiFactorResolverKey] as! MultiFactorResolver
//              var displayNameString = ""
//              for tmpFactorInfo in (resolver.hints) {
//                displayNameString += tmpFactorInfo.displayName ?? ""
//                displayNameString += " "
//              }
//            }
//            return
//          }else{
//            print(error?.localizedDescription)
//          }
            let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "LocalizeController") as! LocalizeController
            mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(mainActivity, animated: true, completion: nil)
        }
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        pickerPhone.isHidden = true
        viewPicker.isHidden = true
        txtphone.attributedPlaceholder = NSAttributedString(string: "Phone Number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        if(LanguageManager.shared.currentLanguage == .ar){
            btnPhone.setTitle("استمر", for: .normal)
            lblWelcome.text = "أهلا بك"
            gmail_btn.setTitle("استخدم حساب جوجل", for: .normal)
            btnOneTime.setTitle("استخدم كلمة مرور لمرة واحدة", for: .normal)
            btnArabic.setTitle("عربى (AR)", for: .normal)
            btnEnglish.setTitle("الإنجليزية (EN)", for: .normal)
        }else{
            btnPhone.setTitle("CONTINUE", for: .normal)
            lblWelcome.text = "WELCOME"
            gmail_btn.setTitle("USE GOOGLE ACCOUNT", for: .normal)
            btnOneTime.setTitle("USE ONE TIME PASSWORD", for: .normal)
            btnArabic.setTitle("ARABIC (AR)", for: .normal)
            btnEnglish.setTitle("ENGLISH (EN)", for: .normal)
        }
        
    }
    

    @IBAction func btnEngChange(_ sender: Any) {
        let selectedLanguage: Languages = .en
        LanguageManager.shared.setLanguage(language: selectedLanguage)
        let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(mainActivity, animated: true, completion: nil)
    }
    
    @IBAction func btnArabicChange(_ sender: Any) {
        let selectedLanguage: Languages = .ar

            // change the language
        LanguageManager.shared.setLanguage(language: selectedLanguage)
        let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(mainActivity, animated: true, completion: nil)
        
//        UIApplication.topViewController!.dismiss(animated: true) {
//                    let delegate = UIApplication.shared.delegate as! AppDelegate
//                    delegate.window?.rootViewController =  Helper.initViewControllerWith(identifier: "SplashViewController", and: "")
//                }
    }
    
}

extension String{
    func localizeString() -> String {
        return NSLocalizedString(self, tableName: "Localizeable", bundle: .main, value: self, comment: self)
    }
}
