//
//  MapViewController.swift
//  Miro
//
//  Created by waqar on 27/03/2021.
//

import UIKit
import GoogleMaps
import CoreLocation
import LanguageManager_iOS

class MapViewController: UIViewController , CLLocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    @IBOutlet weak var btnNext: UIButton!
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        latitude = locValue.latitude
        longitude = locValue.longitude
        let camera = GMSCameraPosition.camera(withLatitude:locValue.latitude, longitude: locValue.longitude, zoom: 14.0)
        mapView.camera = camera
        mapView.animate(to: camera)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(LanguageManager.shared.currentLanguage == .ar){
            btnNext.setTitle("التالي", for: .normal)
        }else{
            btnNext.setTitle("NEXT", for: .normal)
        }
        
        let locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let camera = GMSCameraPosition.camera(withLatitude:latitude, longitude: longitude, zoom: 14.0)
        mapView.camera = camera

        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)

        marker.map = mapView
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500), execute: {
            
            let camera = GMSCameraPosition.camera(withLatitude:self.latitude, longitude: self.longitude, zoom: 14.0)
            self.mapView.camera = camera
            self.mapView.animate(to: camera)
            
        })
    }
    
    @IBAction func btnnext(_ sender: Any) {
        let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "SliderViewController") as! SliderViewController
        mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(mainActivity, animated: true, completion: nil)
    }
        
}
