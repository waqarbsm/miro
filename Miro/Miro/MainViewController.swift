//
//  MainViewController.swift
//  Miro
//
//  Created by waqar on 28/03/2021.
//

import UIKit
import LanguageManager_iOS

class MainViewController: UIViewController {

    @IBOutlet weak var viewMatches: UIView!
    @IBOutlet weak var viewFavourite: UIView!
    @IBOutlet weak var viewNews: UIView!
    @IBOutlet weak var viewSettings: UIView!
    
    @IBOutlet weak var lblMatches: UILabel!
    @IBOutlet weak var lblFavourite: UILabel!
    @IBOutlet weak var lblNews: UILabel!
    @IBOutlet weak var lblSettings: UILabel!
    @IBOutlet weak var lblMain: UILabel!
    
    @IBOutlet weak var btnMyMatches: UIButton!
    @IBOutlet weak var btnAllMatches: UIButton!
    
    @IBOutlet weak var viewHeader: UIView!
    
    @IBAction func btnMyMatchesClick(_ sender: Any) {
        lblMain.text = "My Matches selected"
    }
    
    @IBAction func btnAllMatchesClick(_ sender: Any) {
        lblMain.text = "All Matches selected"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gestureSettings = UITapGestureRecognizer(target: self, action:  #selector(self.openSettings))
        self.viewSettings.addGestureRecognizer(gestureSettings)
        
        let gestureFavourite = UITapGestureRecognizer(target: self, action:  #selector(self.openFavourite))
        self.viewFavourite.addGestureRecognizer(gestureFavourite)
        
        let gestureNews = UITapGestureRecognizer(target: self, action:  #selector(self.openNews))
        self.viewNews.addGestureRecognizer(gestureNews)
        
        let gestureMatches = UITapGestureRecognizer(target: self, action:  #selector(self.openMatches))
        self.viewMatches.addGestureRecognizer(gestureMatches)
        
        lblMatches.textColor = UIColor.red
        lblSettings.textColor = UIColor.white
        lblNews.textColor = UIColor.white
        lblFavourite.textColor = UIColor.white
        
        if(LanguageManager.shared.currentLanguage == .ar){
            lblNews.text = "أخبار"
            lblMain.text = "تم تحديد التطابقات الخاصة بي"
            lblFavourite.text = "المفضل"
            lblMatches.text = "اعواد الكبريت"
            lblSettings.text = "جلسة"
            btnMyMatches.setTitle("المباريات الخاصة بي", for: .normal)
            btnAllMatches.setTitle("كل المباريات", for: .normal)
        }else{
            lblNews.text = "News"
            lblMain.text = "My Matches selected"
            lblFavourite.text = "Favourite"
            lblMatches.text = "Matches"
            lblSettings.text = "Setting"
            btnMyMatches.setTitle("My Matches", for: .normal)
            btnAllMatches.setTitle("All Matches", for: .normal)
        }

    }
    
    @objc func openSettings(sender : UITapGestureRecognizer) {
        lblMatches.textColor = UIColor.white
        lblSettings.textColor = UIColor.red
        lblNews.textColor = UIColor.white
        lblFavourite.textColor = UIColor.white
        if(LanguageManager.shared.currentLanguage == .ar){
            lblMain.text = "تم تحديد إعدادات التذييل"
        }else{
            lblMain.text = "Footer Settings selected"
        }
        viewHeader.isHidden = true
    }
    
    
    @objc func openFavourite(sender : UITapGestureRecognizer) {
        lblMatches.textColor = UIColor.white
        lblSettings.textColor = UIColor.white
        lblNews.textColor = UIColor.white
        lblFavourite.textColor = UIColor.red
        if(LanguageManager.shared.currentLanguage == .ar){
            lblMain.text = "تم تحديد التذييل المفضل"
        }else{
            lblMain.text = "Footer Favourite selected"
        }
        viewHeader.isHidden = true
    }
    
    @objc func openNews(sender : UITapGestureRecognizer) {
        lblMatches.textColor = UIColor.white
        lblSettings.textColor = UIColor.white
        lblNews.textColor = UIColor.red
        lblFavourite.textColor = UIColor.white
        if(LanguageManager.shared.currentLanguage == .ar){
            lblMain.text = "تم تحديد تذييل الأخبار"
        }else{
            lblMain.text = "Footer News selected"
        }
        viewHeader.isHidden = true
    }
    
    @objc func openMatches(sender : UITapGestureRecognizer) {
        lblMatches.textColor = UIColor.red
        lblSettings.textColor = UIColor.white
        lblNews.textColor = UIColor.white
        lblFavourite.textColor = UIColor.white
        if(LanguageManager.shared.currentLanguage == .ar){
            lblMain.text = "تم تحديد التطابقات الخاصة بي"
        }else{
            lblMain.text = "My Matches selected"
        }
        viewHeader.isHidden = false
    }
    
}
