//
//  SliderViewController.swift
//  Miro
//
//  Created by waqar on 27/03/2021.
//

import UIKit
import LanguageManager_iOS

class SliderViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var current = 0
    var total = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl.numberOfPages = 5
        pageControl.currentPage = current
        
        if(LanguageManager.shared.currentLanguage == .ar){
            lblTitle.text = "ألعابك ، حكمك"
            lblDescription.text = "وصفك للشريحة 1"
            btnSkip.setTitle("يتخطى", for: .normal)
            btnNext.setTitle("التالي", for: .normal)
        }else{
            lblTitle.text = "Your Games, Your Rule"
            lblDescription.text = "Your description of slide 1"
            btnSkip.setTitle("SKIP", for: .normal)
            btnNext.setTitle("NEXT", for: .normal)
        }
    }
    
    @IBAction func btnSkipClick(_ sender: Any) {
        if(current == 0){
            return
        }
        current = current - 1
        pageControl.currentPage = current
        let count = current + 1
        if(LanguageManager.shared.currentLanguage == .ar){
            lblDescription.text = "وصفك للشريحة \(count)"
        }else{
            lblDescription.text = "Your description of slide \(count)"
        }
    }
    
    @IBAction func btnNextClick(_ sender: Any) {
        if(current >= total-1){
            let mainActivity = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            mainActivity.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.present(mainActivity, animated: true, completion: nil)
        }
        current = current + 1
        pageControl.currentPage = current
        if(LanguageManager.shared.currentLanguage == .ar){
            lblDescription.text = "وصفك للشريحة \(current)"
        }else{
            lblDescription.text = "Your description of slide \(current)"
        }
        let count = current + 1
        if(LanguageManager.shared.currentLanguage == .ar){
            lblDescription.text = "وصفك للشريحة \(count)"
        }else{
            lblDescription.text = "Your description of slide \(count)"
        }
    }
    
    
    
}
